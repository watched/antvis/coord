#### 0.3.0 (2020-05-12)

##### Chores

- **deps:** 升级 matrix-util ([8b53c642](https://github.com/antvis/coord/commit/8b53c6426def5384593c3ef46c0e1b5530f61777))

#### 0.2.2 (2019-09-26)

##### Chores

- **\*:**
  - script update ([e92f9725](https://github.com/antvis/coord/commit/e92f9725dcff696e661a9cfbe31786671d4ab596))
  - script update ([7db5e2a9](https://github.com/antvis/coord/commit/7db5e2a925d4610080ac9c01f493cd73c68207d0))

##### New Features

- define readonly properties. ([c7915d5b](https://github.com/antvis/coord/commit/c7915d5ba494a14beee296f3fe4b1a34187ca504))

##### Bug Fixes

- we should not modify the value which set by user. ([6e4d191d](https://github.com/antvis/coord/commit/6e4d191dba7816d9a69ae233b93b6b3ac8e4f62a))
- type define bug ([8529a067](https://github.com/antvis/coord/commit/8529a0678f753b8afd6b6eaf5c5d0aed9d81cf72))
- **lint:** code format & fix tslint error ([dd54c307](https://github.com/antvis/coord/commit/dd54c307e33f09aacd86526b17729f7cd6356f4b))

##### Other Changes

- **\*:** add matrix-util dependency ([b89c7493](https://github.com/antvis/coord/commit/b89c7493a58c6b37d1cb1aa64b2fa8651c45c4bc))

##### Refactors

- 重新梳理逻辑, closed [#3](https://github.com/antvis/coord/pull/3) ([067559c0](https://github.com/antvis/coord/commit/067559c0c0303910123bec3a16e4e334051466c6))

##### Tests

- modify test case. ([58c0e868](https://github.com/antvis/coord/commit/58c0e8689d469a5a2f32afacadaf31722a056ddb))
